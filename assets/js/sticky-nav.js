(function() {
  // desktop navbar element
  var mainNavElem = document.getElementById('main-nav')
  //nav desktop child element
  var mainNavContentElem = mainNavElem.querySelector('.main-nav-content')
  // set desktop navbar height to fill empty space after sticky
  var mainNavContentHeight = mainNavContentElem.getBoundingClientRect().height

  // set window current scroll top position
  var windowScrollY = window.scrollY || window.pageYOffset

  // the first slide top position
  var firstSlideTop = document.getElementById('d1-slide').getBoundingClientRect().top + window.scrollY

  // all slide section elements
  var slides = document.querySelectorAll('section.section-slide')
  var slideCheckpoints = []

  // initialise the current slide on screen to be index 0
  var currentSlide = 0

  var onScroll = function () {
    // update window current scroll top position
    windowScrollY = window.scrollY || window.pageYOffset
    // update currentSlide index
    currentSlide = slideCheckpoints.filter(h => windowScrollY >= h).length - 1
    // update first slide top position
    firstSlideTop = document.getElementById('d1-slide').getBoundingClientRect().top + windowScrollY

    // if desktop navbar exceed top screen, set sticky class to its child element
    $(mainNavContentElem).toggleClass('sticky', mainNavElem.getBoundingClientRect().top <= 0)

    // all desktop navbar <a> elements
    var navDesktopAnchor = document.querySelectorAll('.view-desktop ul li a')
    // all mobile navbar <a> elements
    var navMobileAnchor = document.querySelectorAll('.view-mobile ul li a')

    // set highlight class active if one of them match currentSlide index
    for (var i = 0; i < navDesktopAnchor.length; i++) {
      $(navDesktopAnchor[i]).toggleClass('active', i === currentSlide)
    }
    for (var i = 0; i < navMobileAnchor.length; i++) {
      $(navMobileAnchor[i]).toggleClass('active', i === currentSlide)
    }
  }

  var onResize = function () {
    // update navbar desktop height to fill the empty space after sticky
    mainNavContentHeight = mainNavContentElem.getBoundingClientRect().height
    mainNavElem.style.height = mainNavContentHeight + 'px'

    // update each slide position info into checkpoint
    var checkpoint = firstSlideTop
    for (var i = 0; i < slides.length; i++) {
      slideCheckpoints[i] = checkpoint
      checkpoint += slides[i].getBoundingClientRect().height + 20
    }
  }


  window.addEventListener('scroll', onScroll)
  window.addEventListener('resize', onResize)
  onScroll()
  onResize()
})()