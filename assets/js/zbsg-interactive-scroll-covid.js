// Bottom Nav Swing Out
var VanillaRunOnDomReady = function() {
myID = document.getElementById("navbar-bottom");
var myScrollFunc = function () {
	var y = window.scrollY;
	if (y >= 20) {
		myID.className = "longing_nav bottomMenu show"
	} else {
		myID.className = "longing_nav bottomMenu hide"
	}
};
window.addEventListener("scroll", myScrollFunc);
}

var alreadyrunflag = 0;

if (document.addEventListener)
	document.addEventListener("DOMContentLoaded", function(){
		alreadyrunflag=1; 
		VanillaRunOnDomReady();
	}, false);
else if (document.all && !window.opera) {
	document.write('<script type="text/javascript" id="contentloadtag" defer="defer" src="javascript:void(0)"><\/script>');
	var contentloadtag = document.getElementById("contentloadtag")
	contentloadtag.onreadystatechange=function(){
		if (this.readyState=="complete"){
				alreadyrunflag=1;
				VanillaRunOnDomReady();
		}
	}
}